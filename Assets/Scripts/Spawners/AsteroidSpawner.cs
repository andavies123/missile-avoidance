﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteroidSpawner : MonoBehaviour {

    public GameObject asteroidPrefab;
    public GameObject warningPrefab;

    public float minAsteroidSize, maxAsteroidSize, asteroidSpawnTime, warningTime;
    private static float screenWidth, screenHeight;

    private void Start() {
        screenHeight = Camera.main.orthographicSize * 2f;
        screenWidth = screenHeight * Camera.main.aspect;

        FindObjectOfType<GameOverScript>().GameOver += OnGameOver;

        StartCoroutine("AsteroidSpawning");
    }



    IEnumerator AsteroidSpawning() {
        while (true) {
            Vector2 spawnPosition;
            float rotationAngle;
            GetRandomSpawn(out spawnPosition, out rotationAngle);
            GameObject warning = Instantiate(warningPrefab, new Vector3(Mathf.Clamp(spawnPosition.x, screenWidth/-2 + 1, screenWidth/2-1), Mathf.Clamp(spawnPosition.y, screenHeight/-2+1, screenHeight/2-1), 0), transform.rotation);
            StartCoroutine(SpawnAsteroid(warning, spawnPosition, rotationAngle));
            yield return new WaitForSeconds(asteroidSpawnTime);
        }
    }

    IEnumerator SpawnAsteroid(GameObject warning, Vector2 spawnPosition, float rotationAngle) {
        yield return new WaitForSeconds(warningTime);
        Destroy(warning);
        float randomScale = Random.Range(minAsteroidSize, maxAsteroidSize);
        GameObject asteroid = Instantiate(asteroidPrefab, new Vector3(spawnPosition.x, spawnPosition.y, 0), Quaternion.Euler(new Vector3(0, 0, rotationAngle + 180)));
        asteroid.transform.localScale = new Vector3(randomScale, randomScale, 1);
        asteroid.transform.parent = transform;
    }

    private void OnGameOver() {
        FindObjectOfType<GameOverScript>().GameOver -= OnGameOver;
        StopCoroutine("SpawnAsteroid");
    }

    static void GetRandomSpawn(out Vector2 spawnPosition, out float rotationAngle) {
          switch (Random.Range (0, 4)) {
              case 0: // Spawn above
                  spawnPosition.y = -screenHeight / 2;
                  spawnPosition.x = (Random.Range (-screenWidth / 2, screenWidth / 2));
                  rotationAngle = Random.Range (225, 315);
                  break;
              case 1: // Spawn below
                  spawnPosition.y = screenHeight / 2;
                  spawnPosition.x = (Random.Range (-screenWidth / 2, screenWidth / 2));
                  rotationAngle = Random.Range (45, 135);
                  break;
              case 2: // Spawn left
                  spawnPosition.x = -screenWidth / 2;
                  spawnPosition.y = (Random.Range (-screenHeight / 2, screenHeight / 2));
                  rotationAngle = Random.Range (135, 225);
                  break;
              case 3: // Spawn right
                  spawnPosition.x = screenWidth / 2;
                  spawnPosition.y = (Random.Range (-screenHeight / 2, screenHeight / 2));
                  rotationAngle = Random.Range (-45, 45);
                  break;
              default:
                  spawnPosition.x = 0;
                  spawnPosition.y = 0;
                  rotationAngle = 0;
                  break;
          }
    }
}