﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileSpawner : MonoBehaviour {

	public static int MissilesDestroyed;

	public float startingSpawnTimer;
	public float minSpawnTimer;
	public GameObject missilePrefab;
	public GameObject player;

	private static float screenHeight;
	private static float screenWidth;
	private float spawnTimer;

	private float SpawnTimer {
		get { return spawnTimer; }
		set { spawnTimer = Mathf.Clamp(value, minSpawnTimer, startingSpawnTimer);}
	}

	void Start () {
        MissilesDestroyed = 0;
		screenHeight = Camera.main.orthographicSize * 2f;
		screenWidth = screenHeight * Camera.main.aspect;
		SpawnTimer = startingSpawnTimer;
        FindObjectOfType<GameOverScript>().GameOver += OnGameOver;
        StartCoroutine ("SpawnMissile");
	}


	void Update () {}

	static void GetRandomMissileSpawn(out float spawnX, out float spawnY) {
		switch (Random.Range (0, 4)) {
		case 0: // Spawn above
			spawnY = -screenHeight/2;
			spawnX = (Random.Range (-screenWidth/2, screenWidth/2));
			break;
		case 1: // Spawn below
			spawnY = screenHeight/2;
			spawnX = (Random.Range (-screenWidth/2, screenWidth/2));
			break;
		case 2: // Spawn left
			spawnX = -screenWidth/2;
			spawnY = (Random.Range (-screenHeight/2, screenHeight/2));
			break;
		case 3: // Spawn right
			spawnX = screenWidth/2;
			spawnY = (Random.Range (-screenHeight/2, screenHeight/2));
			break;
		default:
			spawnX = 0;
			spawnY = 0;
			break;	
		}
	}

	IEnumerator SpawnMissile() {
		while (true) {
			float spawnX, spawnY;
			GetRandomMissileSpawn (out spawnX, out spawnY);
			GameObject missile = Instantiate (missilePrefab, new Vector3 (spawnX, spawnY, 0), Quaternion.LookRotation (Vector3.forward, player.transform.position - new Vector3 (0, 0, 0)));
			missile.GetComponent<MissileController> ().Player = player;
			missile.transform.parent = this.transform;
			SpawnTimer -= 0.1f;
			yield return new WaitForSeconds (SpawnTimer);
		}
	}

	void OnGameOver() {
		StopCoroutine ("SpawnMissile");
        FindObjectOfType<GameOverScript>().GameOver -= OnGameOver;
    }
}