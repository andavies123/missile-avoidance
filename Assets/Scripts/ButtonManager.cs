﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {
    public void PlayButtonPressNormal() { AudioManager.instance.Play("ButtonPressNormal"); }
    public void PlayButtonPressPlay() { AudioManager.instance.Play("ButtonPressPlay"); }
    public void PlayButtonPressBack() { AudioManager.instance.Play("ButtonPressBack"); }
    public void PlayButtonHover() { AudioManager.instance.Play("ButtonHover"); }
}
