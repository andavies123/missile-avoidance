﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class TimeTextScript : MonoBehaviour {

    public string timerText;

    void Start() {
        
    }

    void Update() {
        GetComponent<TextMeshProUGUI>().text = timerText + TimerScript.timeElapsed.ToString("0.0");
    }
}
