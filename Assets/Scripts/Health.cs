﻿using System;
using UnityEngine;

public class Health : MonoBehaviour {
    public event Action OnDeath;

    public float startingHealth = 1;

    private float _currentHealth;
	public float CurrentHealth { 
        get { return _currentHealth; }
        set {
            _currentHealth = value;
            if (_currentHealth <= 0) OnDeath();
        }
    }

	void Start () { CurrentHealth = startingHealth; }

	public void Hurt(float health) { CurrentHealth -= health; }
	public void Heal(float health) { CurrentHealth += health; }
	public void Kill() { Hurt(CurrentHealth); }
}