﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {

	public float speed;

	public Rigidbody2D RBody { get; set; }

	void Start() {
		RBody = GetComponent<Rigidbody2D> ();
	}

	public void Move(Vector2 velocity) {
        if (RBody == null) return;
		RBody.MovePosition (RBody.position + (velocity * speed * Time.fixedDeltaTime));
	}

	public void MoveBoxConstraint(Vector2 velocity, Vector2 topLeftConstraint, Vector2 bottomRightConstraint) {
		Vector2 newPosition = RBody.position + (velocity * speed * Time.fixedDeltaTime);
		newPosition.x = Mathf.Clamp (newPosition.x, topLeftConstraint.x, bottomRightConstraint.x);
		newPosition.y = Mathf.Clamp (newPosition.y, topLeftConstraint.y, bottomRightConstraint.y);
		RBody.MovePosition (newPosition);
	}
}
