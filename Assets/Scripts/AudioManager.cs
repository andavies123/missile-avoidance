﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    public Sound[] sounds;

    void Awake() {
        if (instance == null)
            instance = this;
        else {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound sound in sounds) {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }

    void Start() {
        Play("BackgroundMusic");
    }

    void Update() {
        foreach(Sound sound in sounds) {
            switch (sound.soundType) {
                case Sound.SoundType.Music: sound.source.volume = VolumeHandler.musicVolume; break;
                case Sound.SoundType.SoundEffect: sound.source.volume = VolumeHandler.soundEffectsVolume; break;
                case Sound.SoundType.GUI: sound.source.volume = VolumeHandler.GUIVolume; break;
            }
        }
    }

    public void Play(string name) {
        Sound sound = Array.Find(sounds, s => s.name == name);
        if(sound == null) {
            Debug.LogError("Sound " + name + " can't be found");
            return;
        }
        sound.source.Play();
    }

    public void Stop(string name) {
        Sound sound = Array.Find(sounds, s => s.name == name);
        if (sound == null) {
            Debug.LogError("Sound " + name + " can't be found");
            return;
        }
        sound.source.Stop();
    }
}
