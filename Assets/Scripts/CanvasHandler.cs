﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasHandler : MonoBehaviour {

    public Canvas mainCanvas;
    public Canvas settingsCanvas;
    public Canvas aboutCanvas;

    private void Start () {
        SwitchToMainCanvas();
    }

    public void SwitchToSettingsCanvas() {
        mainCanvas.gameObject.SetActive(false);
        settingsCanvas.gameObject.SetActive(true);
        aboutCanvas.gameObject.SetActive(false);
    }
    
    public void SwitchToMainCanvas() {
        mainCanvas.gameObject.SetActive(true);
        settingsCanvas.gameObject.SetActive(false);
        aboutCanvas.gameObject.SetActive(false);
    }
    
    public void SwitchToAboutCanvas() {
        mainCanvas.gameObject.SetActive(false);
        settingsCanvas.gameObject.SetActive(false);
        aboutCanvas.gameObject.SetActive(true);
    }
}
