﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameOverScript : MonoBehaviour {

    public event Action GameOver;

    void Start() {
        FindObjectOfType<PlayerController>().GetComponent<Health>().OnDeath += OnGameOver;
    }

    void OnGameOver() { if(GameOver != null) GameOver(); }
}
