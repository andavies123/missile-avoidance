﻿using UnityEngine;

public class GameStateHandler : MonoBehaviour {

    public static GameStateHandler GAME_STATE_HANDLER;
    
    public enum State { Play, Pause, GameOver }

    public Canvas GUICanvas;
    public Canvas PauseCanvas;
    public Canvas GameOverCanvas;

    public KeyCode pauseKey;

    private State _currentState;
    public State CurrentState {
        get {
            return _currentState;
        }
        set {
            _currentState = value;
            switch(value) {
                case State.Play:
                    OnPlay(); break;
                case State.Pause:
                    OnPause(); break;
                case State.GameOver:
                    OnGameOver(); break;
            }
        }
    }

    void Start() {
        CurrentState = State.Play;
        GAME_STATE_HANDLER = this;
    }

    private void Update () {
        if (Input.GetKeyDown(pauseKey) && CurrentState == State.Play)
            CurrentState = State.Pause;
        else if (Input.GetKeyDown(pauseKey) && CurrentState == State.Pause)
            CurrentState = State.Play;
    }

    public void Play() {
        CurrentState = State.Play;
    }

    public void Pause() {
        CurrentState = State.Pause;
    }

    void OnPlay () {
        SetActiveCanvas(true, false, false);
        Time.timeScale = 1;
    }
    
    void OnPause() {
        SetActiveCanvas(false, true, false);
        Time.timeScale = 0;
    }
    
    void OnGameOver() {
        SetActiveCanvas(false, false, true);
    }
    
    void SetActiveCanvas(bool guiActive, bool pauseActive, bool gameOverActive) {
        if (GUICanvas == null || PauseCanvas == null || GameOverCanvas == null) return;
        GUICanvas.enabled = guiActive;
        PauseCanvas.enabled = pauseActive;
        GameOverCanvas.enabled = gameOverActive;
    }
}
