﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HealthBarScript : MonoBehaviour {

    private Slider healthBar;

    private Health playerHealthScript;

    void Start() {
        healthBar = GetComponent<Slider>();
        playerHealthScript = FindObjectOfType<PlayerController>().GetComponent<Health>();
    }
    
    void Update() {
        healthBar.value = playerHealthScript.CurrentHealth / playerHealthScript.startingHealth * 100;
    }
}
