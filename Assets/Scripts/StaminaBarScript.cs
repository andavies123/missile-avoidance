﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class StaminaBarScript : MonoBehaviour {

    private Slider staminaBar;
    private PlayerController player;

    void Start() {
        staminaBar = GetComponent<Slider>();
        player = FindObjectOfType<PlayerController>();
    }
    
    void Update() {
        staminaBar.value = player.SprintTimer / player.sprintDuration * 100;
    }
}
