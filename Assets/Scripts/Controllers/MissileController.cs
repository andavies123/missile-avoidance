﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : Controller {

	public float minSpeed, maxSpeed, minRotationSpeed, maxRotationSpeed, damage;
	public GameObject Player { get; set; }

	private float rotationSpeed;
	private int roamingAngle;
	private bool roaming;

	private float roamingTimer, minRoamingTimer = 5, maxRoamingTimer = 20;

	public override void Start () {
        base.Start();
		movementScript.speed = Random.Range (minSpeed, maxSpeed);
		rotationSpeed = Random.Range (minRotationSpeed, maxRotationSpeed);
		roaming = false;
        FindObjectOfType<GameOverScript>().GameOver += OnDeath;
        StartCoroutine ("SwitchRoam");
	}

	protected override void UpdateRotation() {
		Quaternion newRotation;
		if (roaming) newRotation = Quaternion.Euler (new Vector3 (0, 0, roamingAngle));
		else newRotation = Quaternion.LookRotation (Vector3.forward, Player.transform.position - transform.position) * Quaternion.Euler(0, 0, 90);
		transform.rotation = Quaternion.RotateTowards(transform.rotation, newRotation, rotationSpeed * Time.deltaTime);
	}

	protected override void UpdateDirection() {}
    protected override void Move() { movementScript.Move(transform.right); }

    void OnCollisionEnter2D(Collision2D otherObject) {
        if(otherObject.gameObject.GetComponent<AsteroidController>() && !isDead) {
            MissileSpawner.MissilesDestroyed++;
            healthScript.Kill();
        }
        else if(otherObject.gameObject.GetComponent<PlayerController>() && !isDead) {
            otherObject.gameObject.GetComponent<Health>().Hurt(damage);
            healthScript.Kill();
        }
        else if(otherObject.gameObject.GetComponent<MissileController>() && !isDead) healthScript.Kill();
	}

	IEnumerator SwitchRoam() {
		while (true) {
            roamingTimer = Random.Range(minRoamingTimer, maxRoamingTimer);
            yield return new WaitForSeconds (roamingTimer);
			if (roaming) roaming = false;
			else {
				roaming = true;
				roamingAngle = Random.Range (0, 360);
			}
		}
	}

    protected override void OnDeath() {
        isDead = true;
        FindObjectOfType<AudioManager>().Play("ExplosionMissile");
        animatorScript.SetTrigger("OnDeath");
        healthScript.OnDeath -= OnDeath;
        FindObjectOfType<GameOverScript>().GameOver -= OnDeath;
        Destroy(gameObject, animatorScript.GetCurrentAnimatorStateInfo(0).length * 2);
    }
}