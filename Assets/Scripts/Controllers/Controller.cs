﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(Health))]
[RequireComponent(typeof(Animator))]
public abstract class Controller : MonoBehaviour {
	protected Health healthScript;
	protected Movement movementScript;
	protected Animator animatorScript;

	public bool isDead;
    protected Vector3 direction;

	public virtual void Start () {
		healthScript = GetComponent<Health> ();
		movementScript = GetComponent<Movement> ();
		animatorScript = GetComponent<Animator> ();

        isDead = false;
        direction = transform.right;

		healthScript.OnDeath += OnDeath;
	}

    public virtual void Update() {
        if (isDead) return;
        UpdateDirection();
        UpdateRotation();
        Move();
    }

    protected abstract void UpdateDirection ();
	protected abstract void UpdateRotation ();
    protected abstract void Move();
	protected abstract void OnDeath ();
}
