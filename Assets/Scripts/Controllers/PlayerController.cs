﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Controller {

	public float sprintMultiplier, sprintDuration;

	private float _sprintTimer;
	public float SprintTimer {
		get { return _sprintTimer; }
		set { _sprintTimer = Mathf.Clamp (value, 0, sprintDuration); }
	}

    private bool isSprinting, rechargingSprint;
	private Vector2 topLeftScreenPoint, bottomRightScreenPoint;

	public override void Start () {
        base.Start();
		SprintTimer = sprintDuration;
		topLeftScreenPoint = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0));
		bottomRightScreenPoint = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1));
	}

	public override void Update () {
        base.Update();
        UpdateRecharge();
	}

    protected override void UpdateDirection() { direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized; }
    protected override void UpdateRotation() {
        if (direction.y == 0 && direction.x == 0) return;
        float angle = ((Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI) - 90) % 360;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
    protected override void Move() {
        if (Input.GetKey(KeyCode.LeftShift) && !rechargingSprint) {
            movementScript.MoveBoxConstraint(direction * sprintMultiplier, topLeftScreenPoint, bottomRightScreenPoint);
            SprintTimer -= Time.deltaTime;
            if (!isSprinting) AudioManager.instance.Play("Boost");
            isSprinting = true;
        }
        else {
            if (Input.GetKey(KeyCode.LeftShift)) AudioManager.instance.Play("BoostError");
            movementScript.MoveBoxConstraint(direction, topLeftScreenPoint, bottomRightScreenPoint);
            SprintTimer += Time.deltaTime;
            isSprinting = false;
            AudioManager.instance.Stop("Boost");
        }
    }

	void UpdateRecharge() {
		if (SprintTimer <= 0) rechargingSprint = true;
		else if (SprintTimer >= sprintDuration) rechargingSprint = false;
	}

    void OnCollisionEnter2D(Collision2D otherObject) {
        if(otherObject.gameObject.GetComponent<AsteroidController>() && !otherObject.gameObject.GetComponent<Controller>().isDead) healthScript.Kill();
    }

    protected override void OnDeath() {
		if (isDead) return;
        isDead = true;
		animatorScript.SetTrigger ("OnDeath");
        FindObjectOfType<AudioManager>().Play("ExplosionPlayer");
        GetComponent<Health> ().OnDeath -= OnDeath;
        Destroy (gameObject, animatorScript.GetCurrentAnimatorStateInfo (0).length * 2);
        GameStateHandler.GAME_STATE_HANDLER.CurrentState = GameStateHandler.State.GameOver;
	}
}