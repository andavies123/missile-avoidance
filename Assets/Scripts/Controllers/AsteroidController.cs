﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : Controller {

	public int despawnTimer;

	public override void Start() {
		base.Start ();
        FindObjectOfType<GameOverScript>().GameOver += OnDeath;
        StartCoroutine ("Despawn");
	}

	void OnCollisionEnter2D(Collision2D otherObject) {
        if (otherObject.gameObject.GetComponent<AsteroidController>()) healthScript.Kill();
        else if (otherObject.gameObject.GetComponent<MissileController>() && !otherObject.gameObject.GetComponent<Controller>().isDead) {
            transform.localScale = transform.localScale - new Vector3(0.1f, 0.1f, 0);
            if (transform.localScale.x < 0.1f) healthScript.Kill();
        }
    }

	protected override void UpdateDirection () {}
	protected override void UpdateRotation () {}
    protected override void Move() { movementScript.Move(direction); }

    protected override void OnDeath() {
		isDead = true;
        FindObjectOfType<AudioManager>().Play("ExplosionAsteroid");
        animatorScript.SetTrigger ("OnDeath");
        healthScript.OnDeath -= OnDeath;
        FindObjectOfType<GameOverScript>().GameOver -= OnDeath;
        Destroy (gameObject, animatorScript.GetCurrentAnimatorStateInfo(0).length*2);
	}



	IEnumerator Despawn() {
		yield return new WaitForSeconds (despawnTimer);
        FindObjectOfType<GameOverScript>().GameOver -= OnDeath;
        Destroy(gameObject);
    }
}