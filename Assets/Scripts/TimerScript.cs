﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour {

    public static float timeElapsed;

    void Start() {
        timeElapsed = 0;
    }
    
    void Update() {
        if(GameStateHandler.GAME_STATE_HANDLER.CurrentState != GameStateHandler.State.GameOver)
            timeElapsed += Time.deltaTime;
    }
}
