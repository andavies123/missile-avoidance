﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ScoreHandler : MonoBehaviour {

	public string scoreText;

	void Start () {
		
	}

	void Update () {
		GetComponent<TextMeshProUGUI>().text = scoreText + MissileSpawner.MissilesDestroyed;
	}
}
