﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound  {

    public enum SoundType { Music, SoundEffect, GUI };

    public string name;
    public AudioClip clip;

    /* Not actually using this volume now*/
    [Range(0f, 1f)]
    public float volume;

    [Range(.1f, 3f)]
    public float pitch;

    public bool loop;

    [HideInInspector]
    public AudioSource source;

    public SoundType soundType;
}
