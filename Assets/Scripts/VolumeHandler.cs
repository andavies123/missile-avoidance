﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class VolumeHandler : MonoBehaviour {

    public static float musicVolume = 1;
    public static float soundEffectsVolume = 1;
    public static float GUIVolume = 1;

    public void UpdateMusicVolume(float sliderValue) {
        musicVolume = sliderValue;
    }

    public void UpdateSoundEffectVolume(float sliderValue) {
        soundEffectsVolume = sliderValue;
    }

    public void UpdateGUIVolume(float sliderValue) {
        GUIVolume = sliderValue;
    }
}
